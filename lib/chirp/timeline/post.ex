defmodule Chirp.Timeline.Post do
  use Ecto.Schema
  import Ecto.Changeset

  schema "posts" do
    field :body, :string
    field :likes_count, :integer, default: 0
    field :repos_count, :integer, default: 0
    field :username, :string, default: "anon"

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:body, :username])
    |> validate_required([:body])
    |> validate_length(:body, min: 2, max: 250)
    |> validate_length(:username, min: 2, max: 32)
  end
end
